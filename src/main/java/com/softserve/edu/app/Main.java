package com.softserve.edu.app;

public class Main {

    public static void main(String[] args) {

        // checking for arguments validity
        switch(args.length) {

            case 0: // no arguments, typing instructions
                typeInstructions();
                return;
            case 1: // only one argument
                wrongParameters();
                return;
            case 2:
                // normal program flow
                try {
                    int height = Integer.parseInt(args[0]);
                    int length = Integer.parseInt(args[1]);
                    ChessBoardDrawer.getDrawer(height, length).draw();
                } catch(NumberFormatException e) {
                    System.out.println(e.getMessage());
                } catch (IllegalArgumentException e) {
                    System.out.println(e.getMessage());
                }
                break;
            default:
                System.out.println(args.length + " parameters have been passed, only first two will be considered.");

        }

    }

    static private void typeInstructions() {
        System.out.println("Draws chess board. First param: height, second length");
    }

    static private void wrongParameters() {
        System.out.println("Parameters are invalid");
    }

}
