package com.softserve.edu.app;

public class ChessBoardDrawer {

    private String[] lines = new String[2]; // two rows of chess board will be kept here
    private StringBuilder sb = new StringBuilder();
    private int height;
    private int length;

    private ChessBoardDrawer(int h, int l) {

        height = h;
        length = l;

        for(int i = 0; i < l; i++) {
            if(i % 2 == 0) sb.append('*'); else sb.append(' ');
        }

        lines[0] = sb.toString();
        sb.setLength(0); // clear buffer

        for(int i = 0; i < l; i++) {
            if(i % 2 == 1) sb.append('*'); else sb.append(' ');
        }
        lines[1] = sb.toString();
        sb.setLength(0);

    }

    private static void validateArguments(int a, int b) {
        if(a < 0 || b < 0) {
            throw new IllegalArgumentException("Bad parameters");
        }
    }

    public static ChessBoardDrawer getDrawer(int h, int l) {

        validateArguments(h,l);

        return new ChessBoardDrawer(h, l);

    }

    public String draw() {

        for(int i = 0; i < height; i++) {

            sb.append(lines[i%2]); // drawing different kind of row each time ( * * * * or * * * * *)
            sb.append('\n');

        }

        return sb.toString();

    }

}
