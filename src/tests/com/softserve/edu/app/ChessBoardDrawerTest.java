package com.softserve.edu.app;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

public class ChessBoardDrawerTest {

    @org.junit.Test(expected = IllegalArgumentException.class)
    public void getDrawer() {

        ChessBoardDrawer.getDrawer(-9, 10);

    }

    @org.junit.Test
    public void getDrawer_Mormal() {

        ChessBoardDrawer.getDrawer(10, 10);

    }

    @org.junit.Test
    public void draw() {

        String exp = "* * \n" +
                " * *\n";

        assertEquals(exp, ChessBoardDrawer.getDrawer(2,4).draw());

    }

    @org.junit.Test
    public void draw_0() {

        String exp = "";

        assertEquals(exp, ChessBoardDrawer.getDrawer(0,0).draw());

    }


}
